/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Login from './src/Login/index';
import Home from './src/Home/index';
import Profile from './src/Profile/index';
import Detail from './src/Detail/index';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
const AuthStack = createStackNavigator();
const App = () => {
  return (
    <NavigationContainer>
      <AuthStack.Navigator>
        <AuthStack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <AuthStack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
        <AuthStack.Screen
          name="Detail"
          component={Detail}
          options={{
            headerTransparent: true,
            headerTintColor: 'white',
          }}
        />
        <AuthStack.Screen
          name="Profile"
          component={Profile}
          options={{
            headerTransparent: true,
            headerTintColor: 'white',
          }}
        />
      </AuthStack.Navigator>
    </NavigationContainer>
  );
};

export default App;
