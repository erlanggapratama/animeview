import React from 'react';
import {View, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export default (props) => {
  const {children} = props;
  return (
    <LinearGradient colors={['#FF0707', '#000000']} style={styles.container}>
      <View {...props}>{children}</View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 10,
  },
});
