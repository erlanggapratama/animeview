import React from 'react';
import {Text, View, TextInput, Button, Image, StyleSheet} from 'react-native';

export default ({data}) => {
  const {item} = data;
  return (
    <View style={styles.container}>
      <View style={styles.column}>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.textTitle}>{item.title}</Text>
          <Text style={styles.textDate}>{item.release_date}</Text>
        </View>
        <Text style={styles.textProducer}>{item.producer}</Text>
        <View style={{flexDirection: 'row', marginTop: 20}}>
          <Image source={require('../src/assets/star.png')} />
          <Text style={{color: 'white'}}>{item.rt_score}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 10,
  },
  column: {
    flexDirection: 'column',
  },
  textTitle: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
  textDate: {
    color: 'white',
    fontSize: 20,
    paddingLeft: 10,
  },
  textProducer: {
    fontStyle: 'italic',
    color: 'white',
  },
});
