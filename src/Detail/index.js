import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Button,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Container from '../../Component/Container';

export default ({route}) => {
  const {data} = route.params;
  const {item} = data;

  return (
    <Container>
      <Text
        style={{
          marginTop: 50,
          fontSize: 30,
          color: 'white',
          fontWeight: 'bold',
        }}>
        {item.title}
      </Text>
      <View
        style={{
          margin: 10,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text style={{color: 'white', fontStyle: 'italic'}}>
          {item.producer}
        </Text>
        <View style={{flexDirection: 'row'}}>
          <Image source={require('../assets/star.png')} />
          <Text style={{color: 'white'}}>{item.rt_score}</Text>
        </View>
      </View>
      <ScrollView>
        <Text
          style={{
            fontSize: 20,
            textAlign: 'justify',
            margin: 10,
            color: 'white',
          }}>
          {item.description}
        </Text>
      </ScrollView>
    </Container>
  );
};
