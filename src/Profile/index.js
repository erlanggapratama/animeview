import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Button,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import useAsyncEffect from 'use-async-effect';
import AsyncStorage from '@react-native-community/async-storage';
import Container from '../../Component/Container';

export default ({navigation}) => {
  const [profile, setProfile] = React.useState({
    username: '',
    email: '',
    alamat: '',
    gitlab: '',
  });
  useAsyncEffect(async () => {
    try {
      const value = await AsyncStorage.getItem('@storage_key');
      if (value === 'ReactNatErlangga') {
        setProfile({
          username: 'ReactNatErlangga',
          email: 'erlanggadwipratama.if@gmail.com',
          alamat: 'Surabaya',
          gitlab: 'erlanggapratama',
        });
      } else {
        setProfile({
          username: value,
          email: `${value}@email.com`,
          alamat: '',
          gitlab: value,
        });
      }
    } catch (error) {
      console.log(error);
    }
  }, []);
  return (
    <Container>
      <View
        style={{
          marginTop: 100,
          borderRadius: 20,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        {profile.username === 'ReactNatErlangga' ? (
          <Image
            source={require('../assets/me.jpg')}
            style={{
              borderRadius: 20,
              height: 150,
              width: 150,
            }}
          />
        ) : (
          <View style={{height: 100, width: 100}} />
        )}
      </View>
      <Text
        style={{
          top: '8%',
          fontSize: 30,
          color: 'white',
          textAlign: 'center',
          fontWeight: 'bold',
        }}>
        {profile.username}
      </Text>
      <View
        style={{
          borderRadius: 10,
          padding: 10,
          top: '13%',
          backgroundColor: 'white',
        }}>
        <View style={{padding: 5}}>
          <Text>UserName:</Text>
          <Text>{profile.username}</Text>
        </View>
        <View style={{padding: 5}}>
          <Text>Email:</Text>
          <Text>{profile.email}</Text>
        </View>
        <View style={{padding: 5}}>
          <Text>Alamat:</Text>
          <Text>{profile.alamat}</Text>
        </View>
        <View style={{padding: 5}}>
          <Text>Gitlab:</Text>
          <Text>{profile.gitlab}</Text>
        </View>
      </View>
    </Container>
  );
};
