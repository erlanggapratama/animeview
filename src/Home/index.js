import React, {useState} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Button,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import Container from '../../Component/Container';
import Card from '../../Component/Card';

export default ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  React.useEffect(() => {
    fetch('https://ghibliapi.herokuapp.com/films?limit=8')
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.topTitle}>
        <Text style={{fontSize: 25, fontWeight: 'bold', color: 'white'}}>
          AnimeView
        </Text>
        <TouchableOpacity onPress={() => navigation.push('Profile')}>
          <Image source={require('../assets/profile.png')} />
        </TouchableOpacity>
      </View>
      <Text
        style={{
          marginTop: 20,
          fontSize: 30,
          fontWeight: 'bold',
          color: 'white',
          marginBottom: 20,
        }}>
        List Anime
      </Text>
      {isLoading ? (
        <ActivityIndicator size="large" color="#FF0707" />
      ) : (
        <FlatList
          data={data}
          keyExtractor={(item) => item.id}
          renderItem={(item) => (
            <TouchableHighlight
              onPress={() => navigation.push('Detail', {data: item})}>
              <Card data={item} />
            </TouchableHighlight>
          )}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 10,
    backgroundColor: 'black',
  },
  topTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
