import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Container from '../../Component/Container';

export default ({navigation}) => {
  const [user, setUser] = React.useState('');

  return (
    <Container>
      <View style={styles.title}>
        <Text style={{fontSize: 50, fontWeight: 'bold', color: 'white'}}>
          AnimeView
        </Text>
        <Text
          style={{
            maxWidth: 200,
            textAlign: 'center',
            fontSize: 32,
            color: 'white',
            fontWeight: 'bold',
          }}>
          Portal Anime Number One!!
        </Text>
      </View>
      <View style={styles.form}>
        <TextInput
          style={{
            color: 'black',
            backgroundColor: 'white',
            borderRadius: 30,
            textAlign: 'center',
          }}
          value={user}
          onChangeText={(text) => setUser(text)}
          placeholder="username"
        />
        <TextInput
          style={{
            marginTop: 10,
            backgroundColor: 'white',
            borderRadius: 30,
            textAlign: 'center',
          }}
          placeholder="password"
          textContentType="password"
        />
      </View>
      <TouchableOpacity
        style={styles.button}
        onPress={async () => {
          try {
            await AsyncStorage.setItem('@storage_key', user);
          } catch (err) {
            console.log(err);
          }

          navigation.navigate('Home');
        }}>
        <Text style={{textAlign: 'center'}}>Login</Text>
      </TouchableOpacity>
    </Container>
  );
};

const styles = StyleSheet.create({
  title: {
    flexDirection: 'column',
    alignItems: 'center',
    top: '25%',
  },
  form: {
    top: '60%',
    flexDirection: 'column',
  },
  button: {
    top: '70%',
    borderRadius: 50,
    padding: 10,
    backgroundColor: 'red',
  },
});
